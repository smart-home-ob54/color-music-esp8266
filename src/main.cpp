/**
 * Created by Obroten54.
 * Language: C++ (Arduino)
 * Board: ESP8266
 * Mic: MX9614
 * https://gitlab.com/smart-home-ob54/color-music-esp8266
 *
 * License: See /LICENSE.txt
 */
#include <Arduino.h>

#define NUM_LEDS        83              // count of LEDs
#define DI_PIN          D8              // pin connected to the LEDs
#define MIC_PIN         A0              // microphone pin
#define NOISE           620             // higher value = more noises will be ignored, 620 is good for me
#define MIN_LVL         0               // minimum noise level, when 0 LEDs will be active
#define MAX_LVL         44              // maximum noise level, when all LEDs will be active
#define SMOOTHING_LVL   4               // smoothing level, lower value = more smooth
#define CURRENT_LIMIT   5800            // power limit (in mA), 0 for disable limit

#define SERIAL_RATE     115200          // value of baudrate to connect to PC

#define FASTLED_ESP8266_RAW_PIN_ORDER
#define FASTLED_ESP8266_NODEMCU_PIN_ORDER
#define FASTLED_ESP8266_D1_PIN_ORDER

#include <FastLED.h>
#include <Filter.h>

CRGB leds[NUM_LEDS];

int lvl = 0, midLed, top;
uint8_t ledsNum;
ExponentialFilter<long> ADCFilter(SMOOTHING_LVL, 0);

void setup() {
	midLed = NUM_LEDS / 2;
	ledsNum = midLed;
	if (NUM_LEDS % 2 != 0) {
		ledsNum++;
		midLed++;
	}
	top = ledsNum + ledsNum * 0.2; // margin of 20% in sound volume for a better effect

	CFastLED::addLeds<WS2812, DI_PIN, GRB>(leds, NUM_LEDS);

	if (CURRENT_LIMIT > 0)
		FastLED.setMaxPowerInVoltsAndMilliamps(5, CURRENT_LIMIT);

	FastLED.clear(true);
	FastLED.show();

	Serial.begin(SERIAL_RATE);
	Serial.println("Init");
}

void loop() {
	int n, height;

	n = analogRead(MIC_PIN);
	n = abs(1023 - n);  // remove the MX9614 bias of 1.25VDC
	n = (n <= NOISE) ? 0 : abs(n - NOISE); // remove noises
	ADCFilter.Filter(n); // apply the exponential filter to smooth the signal
	lvl = ADCFilter.Current();

	Serial.println(n);

	height = top * (lvl - MIN_LVL) / (long) (MAX_LVL - MIN_LVL);
	if (height < 0L) height = 0;
	else if (height > top) height = top;

	for (uint8_t i = 0; i < ledsNum; i++) {

		CRGB val{};
		byte color = 128;

		if (i >= height) val = CHSV(0, 0, 0);
		else val = CHSV(color - 255 * (i/(float)ledsNum), 255, 255);

		if (midLed + i < NUM_LEDS) {
			leds[midLed + i] = val;
		}
		if (midLed - i >= 0) {
			leds[midLed - i] = val;
		}
	}
	FastLED.show();
}