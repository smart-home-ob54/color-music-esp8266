#!/bin/bash
#Transform from platformio format to Arduino IDE
fail() {
    printf '%s\n' "$1" >&2
    exit "${2-1}"
}

rm -rf ./arduino_ide
mkdir -p ./arduino_ide/color_music_esp8266 || fail "Can't make dir ;("
cd src || fail "Something wrong, where src? :\\"
for f in *.cpp; do
	cp -- "$f" "../arduino_ide/color_music_esp8266/${f%.cpp}.ino"
done
#for f in *.h; do
	#cp -- "$f" "../arduino_ide/color_music_esp8266/$f"
#done
cd ..
sed -i -- 's/#include <Arduino.h>//g' ./arduino_ide/color_music_esp8266/main.ino
mv ./arduino_ide/color_music_esp8266/main.ino ./arduino_ide/color_music_esp8266/color_music_esp8266.ino
echo "----"
echo "Make sure you have installed all the dependent libraries. Arduino IDE project in arduino_ide/color_music_esp8266"
echo "----"
echo "Убедитесь, что вы установили все зависимые библиотеки. Проект Arduino IDE в arduino_ide/color_music_esp8266"
echo "----"
echo "proekt-obroten.ru"
echo "----"